module.exports = {
    // url: function () {
    //     return this.api.launch_url;
    // },

    url: "https://demo.nopcommerce.com",

    elements:
    {
        
        registerlink: "a[href='/register?returnUrl=%2F']",
        firstNamebox: "input[name='FirstName']",
        lastNamebox: "#LastName",
        emailbox: "#Email",
        passwordbox: "#Password",
        confirmpassword: "#ConfirmPassword",
        registerbtn: "#register-button",
        titleHeader: ".page-title h1",

    },

    commands: [
        
        {
            userRegister( fn,ln, mail, password) {
                return this
                .click('@registerlink')
                .setValue('@firstNamebox', fn)
                .setValue('@lastNamebox', ln)
                .setValue('@emailbox', mail)
                .setValue('@passwordbox', password)
                .setValue('@confirmpassword', password)
                .click('@registerbtn');
            }

        }
    ]
}