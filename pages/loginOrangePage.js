module.exports = {
    
    url : function(){
        return this.api.launch_url;
    },


    elements: {
        userName: "#txtUsername[name='txtUsername']",
        password: "#txtPassword[name='txtPassword']",
        loginbtn: "input[class='button']",
        dbtxt: "#content [class='head']",

    },

    commands:
        [
            {
                enteruserNameAnadPassword(username, password) {
                    return this
                    .setValue('@userName', username)
                    .setValue('@password', password)
                    .click('@loginbtn');

                }
            }
        ]
}

